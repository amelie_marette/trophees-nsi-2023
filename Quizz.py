import random

filequestions=open('questions.csv','r',encoding='iso8859-1')
lquestions=filequestions.readlines()
filequestions.close()


## mise en forme de la table
lquestions=lquestions[1:]# enleve la ligne d'entete 

lquestions=[ligne[:-1] for ligne in lquestions] #enleve le \n de la fin 


lquestions=[ligne.split(',') for ligne in lquestions] # sépare les lignes en liste 


def choix_question(questions):
    """
    Récupère au hasard une question dans les questions
    :param questions:(list) La liste de questions
    :return:(list) La question choisie au hasard avec son indice
    :exemple:
    >>>choix_question(("Combien de faces a un carré ?","Quelle est la capitale de la Russie ?"))
    0,6
    """
    assert type(questions)==list, "Questions doit être une liste"
    
    i=random.randint(0,len(questions)-1)
    q=questions[i]
    del questions[i]
    return (q,questions)

def poser_question(q):
    """
    Pose une question et prend en compte la réponse
    :param q:une question
    :return:(bool) Si la réponse est vraie et sinon renvoie son explication
    :exemples:
    >>>poser_question(1)
    Quelle est la capitale  de la Russie ?Paris
    La capitale de la Russie est Moskou
    """
    question=q[0]
    reponse=q[1]
    explication=q[2]
    res=input(question)
    if res==reponse:
        return True
    else:
        return explication
    

def definir_couleur_pion():
    """
    Renvoie la couleur choisi par le joueur parmi toutes les possibilitées
    :exemples:
    >>> definir_couleur_pion()
    "Quelle couleur voulez-vous choisir ? (rouge/bleu/vert/rose/marron)
    rouge
    >>> definir_couleur_pion()
    Quelle couleur voulez-vous ? (rouge/bleu/vert/rose/marron)
    rose
    """
    couleurs = ["rouge", "bleu", "vert", "rose", "marron"]
    couleur=""
    while couleur not in couleurs:
        couleur=input("Quelle couleur voulez-vous choisir ? (rouge/bleu/vert/rose/marron)")
    return couleur

def definir_forme_pion():
    """
    Renvoie la forme choisi par le joueur parmi toutes les possibilitées
    :exemples:
    >>> definir_forme_pion()
    Quelle forme voulez-vous ? (carré/cercle/étoile)
    cercle
    >>> definir_forme_pion()
    Quelle forme voulez-vous ? (carré/cercle/étoile)
    """
    formes = ["carré", "cercle", "étoile"]
    forme=""
    while forme not in formes:
        forme=input("Quelle forme voulez-vous choisir ? (carré/cercle/étoile)")
    return forme


def creer_la_grille(ligne, colonne):
    """
    Crée une grille de 4 lignes et 5 colonnes
    :param ligne:(int) Le nombre de ligne
    :param colonne:(int) Le nombre de colonne
    :exemple:
    >>> creer_la_grille()
    O O O O O
    O O O O O
    O O O O O
    O O O O O
    """
    tab=[]
    for _ in range(4):
        lignes=[]
        for _ in range(5):
            lignes.append("O")
        tab.append(lignes)
    tab[ligne][colonne] = 'X'
    return tab



def afficher_grille(grille):
    """
    Affiche une grille
    :param grille:(list) La grille
    """
    for row in grille:
        print(" ".join(row))
        

def haut(ligne, colonne):
    """
    Renvoie la nouvelle position du pion
    :param ligne:(int) La ligne du pion
    :param colonne:(int) La colonne du pion
    :return:(list) La nouvelle position du pion
    :exemples:
    >>>haut(4,5)
    3,5
    """
    assert type(ligne)==int, "La ligne doit être un entier"
    assert type(colonne)==int, "La colonne doit être un entier"
    ligne-=1
    return (ligne, colonne)

def bas(ligne, colonne):
    """
    Renvoie la nouvelle position du pion
    :param ligne:(int) La ligne du pion
    :param colonne:(int) La colonne du pion
    :return:(list) La nouvelle position du pion
    :exemples:
    >>>bas(4,6)
    (5,6)
    """
    assert type(ligne)==int, "La ligne doit être un entier"
    assert type(colonne)==int, "La colonne doit être un entier"
    ligne+=1
    return (ligne, colonne)
    
def gauche(ligne, colonne):
    """
    Renvoie la nouvelle position du pion
    :param ligne:(int) La ligne du pion
    :param colonne:(int) La colonne du pion
    :return:(list) La nouvelle position du pion
    :exemples:
    >>>gauche(7,2)
    (7,1)
    """
    assert type(ligne)==int, "La ligne doit être un entier"
    assert type(colonne)==int, "La colonne doit être un entier"
    colonne-=1
    return (ligne, colonne)
    
def droite(ligne, colonne):
    """
    Renvoie la nouvelle position du pion
    :param ligne:(int) La ligne du pion
    :param colonne:(int) La colonne du pion
    :return:(list) La nouvelle position du pion
    :exemples:
    >>>droite(4,8)
    (4,9)
    """
    assert type(ligne)==int, "La ligne doit être un entier"
    assert type(colonne)==int, "La colonne doit être un entier"
    colonne+=1
    return (ligne, colonne)

def affiche_deplacement(ligne, colonne):
    """
    Affiche le déplacement du joueur apres lui avoir demander ou il veut aller on fonction de sa position x et y.
    :param ligne:(int)La ligne du pion
    :param colonne:(int)La colonne du pion
    :return:(str) Le deplacement choisi par le joueur
    :exemple:
    >>> afficher_deplacement(3,3)
    Ou voulez-vous vous déplacer, Haut, Gauche ou Droite?Haut
    'Haut'
    >>> afficher_deplacement(1,1)
    Ou voulez-vous vous déplacer, Haut, Bas, Gauche ou Droite?Droite
    'Droite'
    >>> afficher_deplacement(4,2)
    Ou voulez-vous  vous déplacer, Haut, Bas ou Gauche?Gauche
    'Gauche'
    """
    assert type(ligne)==int, "La ligne doit être un entier"
    assert type(colonne)==int, "La colonne doit être un entier"
    
    possibilite = []
    if colonne != 0:
        possibilite.append('Gauche')
    if colonne != 4:
        possibilite.append('Droite')
    if ligne != 0:
        possibilite.append('Haut')
    if ligne != 3:
        possibilite.append('Bas')
    phrase= "Ou voulez-vous vous déplacer (" + ",".join(possibilite) + ") ?"
    reponse=''
    while reponse not in possibilite:
        reponse=input(phrase)
    return reponse

def jouer(ligne, colonne):
    """
    Renvoie les nouvelles coordonnées du pion
    :param ligne:(int)La ligne du pion
    :param colonne:(int)La colonne du pion
    :return:(list) La ligne et  la colonne du pion
    :exemple:
    >>>jouer(4,6) #avec reponse=='Droite'
    (4,7)
    """
    assert type(ligne)==int, "La ligne doit être un entier"
    assert type(colonne)==int, "La colonne doit être un entier"
    
    reponse = affiche_deplacement(ligne, colonne)
    if reponse == 'Gauche':
        (ligne, colonne) = gauche(ligne, colonne)
    elif reponse == 'Droite':
        (ligne, colonne) = droite(ligne, colonne)
    elif reponse == 'Haut':
        (ligne, colonne) = haut(ligne, colonne)
    elif reponse =='Bas':
        (ligne, colonne) = bas(ligne, colonne)
    return ligne, colonne


def nombre_fautes(nbr_fautes):
    """
    Affiche le nombre de fautes commises par le joueur
    :param nbr_fautes:(int) Le nombres de fautes
    :return:(str) La phrase disant au joueur son nombre de fautes
    :exemple:
    >>>nombre_fautes(5)
    Vous avez 5 fautes
    """
    assert type(nbr_fautes)==int, "Le nombre de fautes doit être un entier"
    
    print ("Vous avez " + str(nbr_fautes) + " fautes")
    


def c_est_la_fin(ligne, colonne):
    """
    Renvoie quand c'est la fin de la partie.
    :param ligne:(int)Le nom des lignes
    :param colonne:(int)Le nom des colonnes
    :return:(bool) Vrai ou Faux
    :exemple:
    >>> c_est_la_fin(1,2)
    False
    >>> c_est_la_fin(3,4)
    True
    """
    assert type(ligne)==int, "La ligne doit être un entier"
    assert type(colonne)==int, "La colonne doit être un entier"
    
    if ligne==3 and colonne==4:
        return True
    else:
        return False
    
def afficher_resultat(nbr_fautes):
    """
    Affiche le resultat selon le nombre de fautes
    :param nbr_fautes:(int) Le nombres de fautes
    :return:(str) Le resultat du jeu
    :exemple:
    >>>afficher_resultat(4)
    Vous avez gagné !!!!!
    >>>afficher_resultat(10)
    Vous avez perdu
    """
    assert type(nbr_fautes)==int, "Le nombre de fautes doit être un entier"
    
    if nbr_fautes == 10:
        print("Vous avez perdu")
    else:
        print("Vous avez gagné !!!!!")
    

def jeu():
    """
    Lance le jeu de questions
    :return: None
    """
    nbr_fautes=0
    ligne=0
    colonne=0
    
    couleur = definir_couleur_pion()
    forme = definir_forme_pion()
    grille = creer_la_grille(ligne,colonne)
    
    afficher_grille(grille)
    liste_questions=lquestions
    
    while not c_est_la_fin(ligne, colonne) and nbr_fautes<10:
        q,liste_question=choix_question(liste_questions)
        r=poser_question(q)
        while r!=True:
            print(r)
            nbr_fautes+=1
            q,liste_question=choix_question(liste_questions)
            r=poser_question(q)
        
        ligne,colonne = jouer(ligne, colonne)
        grille = creer_la_grille(ligne,colonne)
        afficher_grille(grille)
    
    afficher_resultat(nbr_fautes)
     
     
if __name__=="__main__":
    jeu()
    